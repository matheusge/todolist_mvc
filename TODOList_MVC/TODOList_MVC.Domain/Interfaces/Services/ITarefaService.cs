﻿using TODOList_MVC.Domain.Entities;

namespace TODOList_MVC.Domain.Interfaces.Services
{
    public interface ITarefaService : IServiceBase<Tarefa>
    {
    }
}
