﻿using TODOList_MVC.Domain.Entities;

namespace TODOList_MVC.Domain.Interfaces.Repositories
{
    public interface ITarefaRepository : IRepositoryBase<Tarefa>
    {
    }
}
