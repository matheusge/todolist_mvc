﻿using TODOList_MVC.Domain.Entities;
using TODOList_MVC.Domain.Interfaces.Repositories;
using TODOList_MVC.Domain.Interfaces.Services;

namespace TODOList_MVC.Domain.Services
{
    public class TarefaService : ServiceBase<Tarefa>, ITarefaService
    {
        private readonly ITarefaRepository _tarefaRepository;

        public TarefaService(ITarefaRepository tarefaRepository)
            : base(tarefaRepository)
        {
            _tarefaRepository = tarefaRepository;
        }
    }
}
