﻿using TODOList_MVC.Domain.Entities;

namespace TODOList_MVC.Application.Interface
{
    public interface ITarefaAppService : IAppServiceBase<Tarefa>
    {
    }
}
