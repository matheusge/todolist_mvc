﻿using TODOList_MVC.Application.Interface;
using TODOList_MVC.Domain.Entities;
using TODOList_MVC.Domain.Interfaces.Services;

namespace TODOList_MVC.Application
{
    public class TarefaAppService : AppServiceBase<Tarefa>, ITarefaAppService
    {
        private readonly ITarefaService _tarefaService;

        public TarefaAppService(ITarefaService tarefaService)
            : base(tarefaService)
        {
            _tarefaService = tarefaService;
        }
    }
}
