﻿
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(TODOList_MVC.CrossCutting.DI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(TODOList_MVC.CrossCutting.DI.App_Start.NinjectWebCommon), "Stop")]
namespace TODOList_MVC.Presentation.API.App_Start
{
    public static class InitializeNinjectWebCommon
    {
    }
}