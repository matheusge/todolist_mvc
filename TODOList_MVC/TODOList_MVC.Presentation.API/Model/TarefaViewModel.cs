﻿namespace TODOList_MVC.Presentation.API.Model
{
    public class TarefaViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Concluida { get; set; }
    }
}