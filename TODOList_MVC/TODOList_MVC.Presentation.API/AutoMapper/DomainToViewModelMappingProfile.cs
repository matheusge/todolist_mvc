﻿using AutoMapper;
using TODOList_MVC.Domain.Entities;
using TODOList_MVC.Presentation.API.Model;

namespace TODOList_MVC.Presentation.API.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<TarefaViewModel, Tarefa>();
        }
    }
}