﻿using AutoMapper;
using TODOList_MVC.Domain.Entities;
using TODOList_MVC.Presentation.API.Model;

namespace TODOList_MVC.Presentation.API.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<Tarefa, TarefaViewModel>();
        }
    }
}