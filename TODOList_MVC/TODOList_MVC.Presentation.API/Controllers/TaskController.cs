﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TODOList_MVC.Application.Interface;
using TODOList_MVC.Domain.Entities;
using TODOList_MVC.Domain.Interfaces.Services;
using TODOList_MVC.Presentation.API.Model;

namespace TODOList_MVC.Presentation.WebAPI.Controllers
{
    public class TaskController : ApiController
    {
        private ITarefaAppService _tarefaAppService;

        public TaskController(ITarefaAppService tarefaAppService)
        {
            _tarefaAppService = tarefaAppService;
        }

        [HttpGet]
        public IEnumerable<TarefaViewModel> Get()
        {
            try
            {
                var tarefasDomain = _tarefaAppService.GetAll();
                var tarefasViewModel = Mapper.Map<IEnumerable<Tarefa>, IEnumerable<TarefaViewModel>>(tarefasDomain);
                return tarefasViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public TarefaViewModel Get(int id)
        {
            try
            {
                var tarefaDomain = _tarefaAppService.GetById(id);
                var tarefaViewModel = Mapper.Map<Tarefa, TarefaViewModel>(tarefaDomain);
                return tarefaViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public void Create([FromBody]TarefaViewModel t)
        {
            try
            {
                Tarefa tarefaDomain = Mapper.Map<TarefaViewModel, Tarefa>(t);
                _tarefaAppService.Add(tarefaDomain);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        public void Update([FromBody]TarefaViewModel t)
        {
            try
            {
                Tarefa tarefaDomain = Mapper.Map<TarefaViewModel, Tarefa>(t);
                var tarefa = _tarefaAppService.GetById(tarefaDomain.Id);
                if(tarefa == null)
                {
                    throw new Exception("Código inválido");
                }

                tarefa.Nome = tarefaDomain.Nome;
                tarefa.Concluida = tarefaDomain.Concluida;

                _tarefaAppService.Update(tarefa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        public void Delete(int id)
        {
            try
            {
                var tarefa = _tarefaAppService.GetById(id);
                if (tarefa == null)
                {
                    throw new Exception("Tarefa não encontrada");
                }

                _tarefaAppService.Remove(tarefa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
