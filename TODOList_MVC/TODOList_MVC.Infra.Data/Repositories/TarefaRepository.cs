﻿using TODOList_MVC.Domain.Entities;
using TODOList_MVC.Domain.Interfaces.Repositories;

namespace TODOList_MVC.Infra.Data.Repositories
{
    public class TarefaRepository : RepositoryBase<Tarefa>, ITarefaRepository
    {
    }
}
