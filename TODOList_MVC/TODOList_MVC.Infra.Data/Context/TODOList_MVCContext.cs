﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TODOList_MVC.Domain.Entities;
using TODOList_MVC.Infra.Data.EntityConfig;
using SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices;

namespace TODOList_MVC.Infra.Data.Context
{
    public class TODOList_MVCContext : DbContext
    {
        public TODOList_MVCContext()
            : base("TODOList_MVC")
        {
        }

        public DbSet<Tarefa> Tarefas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Configurations.Add(new TarefaConfiguration());
        }

    }
}
