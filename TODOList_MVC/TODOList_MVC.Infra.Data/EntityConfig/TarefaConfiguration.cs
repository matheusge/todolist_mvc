﻿using System.Data.Entity.ModelConfiguration;
using TODOList_MVC.Domain.Entities;

namespace TODOList_MVC.Infra.Data.EntityConfig
{
    public class TarefaConfiguration : EntityTypeConfiguration<Tarefa>
    {
        public TarefaConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .IsRequired()
                .HasMaxLength(50);

            Property(x => x.Concluida);
        }
    }
}
